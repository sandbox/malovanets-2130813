<?php

/**
 * @file
 * Administrative interface for Commerce CloudIQ module.
 */

/**
 * Settings form.
 */
function commerce_cloudiq_setup_form($form_state) {
  $form['commerce_cloudiq_api_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Your API ID'),
    '#default_value' => variable_get('commerce_cloudiq_api_id', ''),
    '#required' => TRUE,
  );

  $form['commerce_cloudiq_api_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Your API token'),
    '#default_value' => variable_get('commerce_cloudiq_api_token', ''),
    '#required' => TRUE,
    '#description' => t('Go to Account -> API credentials page to get this credentials.'),
  );

  /*
  $form['commerce_cloudiq_scope'] = array(
    '#type' => 'select',
    '#title' => t('JavaScript scope'),
    '#options' => array(
      'footer' => t('Footer'),
      'header' => t('Header'),
    ),
    '#default_value' => variable_get('commerce_cloudiq_scope', 'header'),
  );
  */

  return system_settings_form($form);
}

/**
 * Implements hook_form_validate().
 */
function commerce_cloudiq_setup_form_validate($form, &$form_state) {
  $xml = commerce_cloudiq_api_call('lookup', 'account', array(), $form_state['values']['commerce_cloudiq_api_id'], $form_state['values']['commerce_cloudiq_api_token']);

  if ($xml['@attributes']['status'] == '101') {
    form_set_error('', $xml['description']);
  }
}

/**
 * Dashboard view.
 */
function commerce_cloudiq_dashboard() {
  $api_id = variable_get('commerce_cloudiq_api_id');
  $api_token = variable_get('commerce_cloudiq_api_token');
  $output = '';

  if (empty($api_id) || empty($api_token)) {
    drupal_set_message(t('Please set up your API credentials to view the dashboard.'), 'warning');
  }
  else {
    $xml = commerce_cloudiq_api_call('store', 'dashboard');
    dsm($xml);
    //$output = '<iframe src="https://platform.cloud-iq.com/?module=dashboard"></iframe>';
  }

  return $output;
}